# README

Chrome/Opera extension (Opera is the main target) that adds a browser button to control amazon, spotify, xbox, and youtube media players without switching tabs.

Feel free to contact me or add an issue for a bug or feature you want.

To build the source, you must have [TypeScript](http://www.typescriptlang.org/) installed.

Check out, run build.bat, and load as an unpacked extension for Chrome or Opera.

## CHANGELOG

v1.6: Add Xbox music player, refactor html template.

v1.5: Simplify and improve data display, fix Youtube buttons.

v1.3.1: Fix bugs, add marquee effect when title is too long to display.

v1.2: Add Amazon Music player compatibility, and other various improvements.

v1.1: Initial release

## CREDITS

[Font Awesome](http://fortawesome.github.io/Font-Awesome/) by Dave Gandy

## LICENSE

Copyright 2015 Matt Murphy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.