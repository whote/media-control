///<reference path="../references/chrome.d.ts" />
///<reference path="../references/lib-extensions.d.ts" />

const enum PlayerKind { None, Spotify, Youtube, Amazon, Xbox };

interface IEventOptions {
	mouseEventType: string;
	refreshEvent: boolean;
	overrides?: any;
}

interface IPlayerStatus {
	paused: boolean;
	next: boolean;
	previous: boolean;
	shuffle?: boolean;
	repeat?: boolean;
	autoplay?: boolean;
	mute?: boolean;
}

class PlayerControls {
	private playPause: HTMLElement;
	private previous: HTMLElement;
	private next: HTMLElement;
	private shuffle: HTMLElement;
	private autoplay: HTMLElement;
	private mute: HTMLElement;
	private repeat: HTMLElement;
	private kind: PlayerKind;
	private playerContainer: HTMLElement;

	constructor(kind: PlayerKind, document: Document) {
		this.kind = kind;
		this.bind(document);
	}

	bind(document: Document): void {
		if (this.kind == PlayerKind.Spotify) {
			this.playPause = document.getElementById("play-pause");
			this.previous = document.getElementById("previous");
			this.next = document.getElementById("next");
			this.shuffle = document.getElementById("shuffle");
			this.repeat = document.getElementById("repeat");
		}

		if (this.kind == PlayerKind.Youtube) {
			this.playPause = <HTMLElement>(document.querySelector("div.ytp-button-play") || document.querySelector("div.ytp-button-pause"));
			this.previous = <HTMLElement>(document.querySelector("a.prev-playlist-list-item") || document.querySelector("div.ytp-button-prev"));
			this.next = <HTMLElement>(document.querySelector("a.next-playlist-list-item") || document.querySelector("div.ytp-button-next"));
			this.shuffle = <HTMLElement>document.querySelector("button.shuffle-playlist");
			this.autoplay = <HTMLElement>(document.querySelector("button.toggle-autoplay") || document.querySelector("button.toggle-loop"));
			this.mute = <HTMLElement>document.querySelector("div.ytp-button-volume");
		}

		if (this.kind == PlayerKind.Amazon) {
			this.playerContainer = document.getElementById("mp3Player");
			this.playPause = <HTMLElement>this.playerContainer.querySelector("span.mp3MasterPlay");
			this.previous = <HTMLElement>this.playerContainer.querySelector("span.mp3PlayPrevious");
			this.next = <HTMLElement>this.playerContainer.querySelector("span.mp3PlayNext"); // disable based on availablity?
			this.shuffle = <HTMLElement>this.playerContainer.querySelector("span.shuffleButton");
			this.repeat = <HTMLElement>this.playerContainer.querySelector("span.repeatButton");
			this.mute = <HTMLElement>this.playerContainer.querySelector("#volumeIcon");
        }

        if (this.kind == PlayerKind.Xbox) {
            this.playPause = <HTMLElement>(document.querySelector("button.iconPlayerPlay") || document.querySelector("button.iconPlayerPause")); // paused || playing
            this.previous = <HTMLElement>document.querySelector("button.iconPlayerPrevious");
            this.next = <HTMLElement>document.querySelector("button.iconPlayerNext");
            this.shuffle = <HTMLElement>(document.querySelector("button.iconPlayerShuffle") || document.querySelector("button.iconPlayerShuffleActive")); // off || on
            this.repeat = <HTMLElement>(document.querySelector("button.iconPlayerRepeat") || document.querySelector("button.iconPlayerRepeatActive")); // off || on
            this.mute = <HTMLElement>(document.querySelector("button[title=Mute]") || document.querySelector("button.iconPlayerMute[title=Unmute]")); // not muted || muted
        }
	}

	get status(): IPlayerStatus {
		if (this.kind == PlayerKind.Spotify) {
			return {
				paused: !this.playPause.classList.contains("playing"),
				shuffle: this.shuffle.classList.contains("active"),
				repeat: this.repeat.classList.contains("active"),
				// spotify doesn't disable based on position
				next: true,
				previous: true,
			};
		}

		if (this.kind == PlayerKind.Youtube) {
			return {
				paused: this.playPause ? this.playPause.classList.contains("ytp-button-play") : false,
				autoplay: this.autoplay ? this.autoplay.classList.contains("yt-uix-button-toggled") : undefined,
				shuffle: this.shuffle ? this.shuffle.classList.contains("yt-uix-button-toggled") : undefined,
				mute: this.mute ? this.mute.dataset["value"] == "off" : false,
				// youtube next/previous have infinite scroll
				next: !!this.autoplay,
				previous: !!this.autoplay,
			};
		}

		if (this.kind == PlayerKind.Amazon) {
			return {
				mute: this.mute.classList.contains("icon-volumeMute"),
				paused: (<HTMLElement>this.playerContainer.querySelector("div.mp3MasterPlayGroup")).classList.contains("paused"),
				shuffle: this.shuffle.classList.contains("shuffled"),
				repeat: this.repeat.classList.contains("repeated"),
				next: (<HTMLElement>this.playerContainer.querySelector("div.mp3MasterPlayGroup")).classList.contains("hasNext"),
				previous: (<HTMLElement>this.playerContainer.querySelector("div.mp3MasterPlayGroup")).classList.contains("hasPrevious"),
			};
        }

        if (this.kind == PlayerKind.Xbox) {
            return {
                mute: this.mute.classList.contains("iconPlayerMute"),
                paused: this.playPause.classList.contains("iconPlayerPlay"),
                shuffle: this.shuffle.classList.contains("iconPlayerShuffleActive"),
                repeat: this.repeat.classList.contains("iconPlayerRepeatActive"),
                next: !this.next.disabled,
                previous: !this.previous.disabled,
            };
        }

		return {paused: true, next: false, previous: false};
	}

	getElement(action: string): Element {
		switch(action) {
			case "previous":
				return this.previous;
			case "next":
				return this.next;
			case "shuffle":
				return this.shuffle;
			case "autoplay":
				return this.autoplay;
			case "mute":
				return this.mute;
			case "repeat":
				return this.repeat;
			default:
				return this.playPause;
		}
	}
}

interface IPlayerData {
	title: string;
	artist: string;
	imageUrl: string;
}

interface IPlayer {
	kind: PlayerKind;
	controls: PlayerControls;
	document: HTMLDocument;
	clickEvent: MouseEvent;

	status(): any;
	trigger(action: string): void;
	update(): void;
}

class BasePlayer implements IPlayer {
	kind: PlayerKind;
	controls: PlayerControls;
	frameWindow: Window;
	clickEvent: MouseEvent;
	data: IPlayerData;
	eventOptions: IEventOptions;

	get document(): HTMLDocument {
		return this.frameWindow.document;
	}

	constructor(frameWindow: Window, eventOptions: IEventOptions = {mouseEventType: "click", refreshEvent: false}) {
		if (!this.kind) this.kind = PlayerKind.None;
		this.eventOptions = eventOptions;
		console.log("initializing with ", this.kind);

		this.frameWindow = frameWindow;
		this.controls = new PlayerControls(this.kind, this.document);
		this.data = {artist: "", imageUrl: "", title: ""};
	}

	update(): void {
		this.controls.bind(this.document);
	}

	status(): any {
		return {
			valid: true,
			data: this.data,
			status: this.controls.status
		};
	}

	trigger(action: string): void {
		var element: Element = this.controls.getElement(action);
		if (element) {
			this.buildEvent(action);
			element.dispatchEvent(this.clickEvent);
		} else {
			console.error("Action not implemented for PlayerKind ", this.kind);
		}
	}

	private buildEvent(action: string): void {
		var eventType: string;
		if (this.eventOptions.refreshEvent || !this.clickEvent) {
			eventType = this.eventOptions.mouseEventType;
			if (this.eventOptions.overrides && this.eventOptions.overrides[action]) eventType = this.eventOptions.overrides[action];
			console.log("create event", eventType, "for", action);
			this.clickEvent = this.document.createEvent("MouseEvent");
			this.clickEvent.initMouseEvent(eventType, true, true, this.frameWindow);
		}
	}

	hasPlayer(): boolean {
		return false;
	}
}

class SpotifyPlayer extends BasePlayer {
	constructor(window: Window) {
		this.kind = PlayerKind.Spotify;
		super(window);
		this.update();
	}

	update(): void {
		var image: HTMLElement = <HTMLElement>(this.document.getElementById("cover-art").querySelector("div.sp-image-img"));
		this.data.artist = this.document.getElementById("track-artist").textContent.trim();
		this.data.imageUrl = image ? image.style.backgroundImage.replace("url(", "").replace(")", "") : "";
		this.data.title = this.document.getElementById("track-name").textContent.trim();
		super.update();
	}

	status(): any {
		if (!this.hasPlayer()) return { valid: false };

		return super.status();
	}

	hasPlayer(): boolean {
		var playPauseControl: HTMLElement = <HTMLElement>this.controls.getElement("playPause");
		return playPauseControl && !playPauseControl.classList.contains("disabled");
	}
}

class XboxPlayer extends BasePlayer {
    constructor(window: Window) {
        this.kind = PlayerKind.Xbox;
        super(window, { mouseEventType: "click", refreshEvent: true });
        this.update();
    }

    update(): void {
		var hasPlayer = this.hasPlayer(),
	        image: HTMLImageElement = <HTMLImageElement>document.querySelector("div.playerNowPlayingImg img.img"),
            defaultImage: HTMLImageElement = <HTMLImageElement>document.querySelector("div.playerNowPlayingImg img.iconDefaultImage"),
            nowPlayingList: NodeList = document.querySelectorAll("div.playerNowPlaying");

        this.data.imageUrl = image && image.style.display === "inline" ? image.src : (defaultImage ? defaultImage.src : "");

        if (!hasPlayer || !nowPlayingList.length) {
            this.data.artist = "n/a";
            this.data.title = "n/a";
        } else {
            var nowPlayingValid: HTMLElement,
                count = nowPlayingList.length;

            for (var index = 0; index < count; index++) {
                if ((<HTMLElement>nowPlayingList[index]).style.display !== "none") {
                    nowPlayingValid = <HTMLElement>nowPlayingList[index];
                    break;
                }
            }

            this.data.artist = nowPlayingValid.querySelector("div.playerNowPlaying[style=''] div.secondaryMetadata > a").textContent.trim();
            this.data.title = nowPlayingValid.querySelector("div.playerNowPlaying[style=''] div.primaryMetadata > a").textContent.trim();
        }

        super.update();
    }

    status(): any {
        if (!this.hasPlayer()) return { valid: false };

        return super.status();
    }

    hasPlayer(): boolean {
        // not sure if this is right, looks like it should though. Copypasta from spotify function
        var playPauseControl: HTMLButtonElement = <HTMLButtonElement>this.controls.getElement("playPause");
        return playPauseControl && !playPauseControl.classList.contains("disabled") && !playPauseControl.disabled;
    }
}

class YoutubePlayer extends BasePlayer {
	parseID: RegExp;
	id: string;

	constructor(window: Window) {
		this.kind = PlayerKind.Youtube;
		super(window, { mouseEventType: "click", refreshEvent: true });
		// http://stackoverflow.com/questions/6903823/regex-for-youtube-id
		this.parseID = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
		this.update();
	}

	update(): void {
		var hasPlayer = this.hasPlayer(),
			userName = this.document.querySelector("a.yt-user-name") || this.document.querySelector("div.yt-user-info"),
			videoTitle = this.document.getElementById("eow-title"),
			videoImage = "http://img.youtube.com/vi/{ID}/default.jpg".replace("{ID}", this.id);

		this.data.title = hasPlayer && videoTitle ? videoTitle.textContent.trim() : "";
		this.data.artist = hasPlayer && userName ? userName.textContent.trim() : "";
		this.data.imageUrl = hasPlayer && videoImage ? videoImage : "";
		super.update();
	}

	status(): any {
		if (!this.hasPlayer()) return { valid: false };

		return super.status();
	}

	hasPlayer(): boolean {
		var player = this.document.getElementById("movie_player"),
			container = this.document.getElementById("player"),
			hasPlayer = player && container ? player.classList.contains("html5-video-player") && !container.classList.contains("off-screen") : false;

		this.id = "";
		if (this.parseID.test(location.href)) {
			this.id = this.parseID.exec(location.href)[1];
		}

		return hasPlayer && (this.id.length > 0);
	}
}

class AmazonPlayer extends BasePlayer {
	constructor(window: Window) {
		var overrides: any = {};
		overrides["mute"] = "click";
		this.kind = PlayerKind.Amazon;
		super(window, { mouseEventType: "mousedown", refreshEvent: true, overrides: overrides});
		this.update();
	}

	update(): void {
		var hasPlayer = this.hasPlayer(),
			container = this.document.getElementById("nowPlayingSection");
		this.data.title = hasPlayer ? container.querySelector("div.currentSongDetails > span.title").textContent.trim() : "";
		this.data.artist = hasPlayer ? container.querySelector("div.currentSongDetails a.artistLink").textContent.trim() : "";
		this.data.imageUrl = hasPlayer ? (<HTMLImageElement>container.querySelector("img.albumImage.small")).src : "";
		super.update();
	}

	status(): any {
		if (!this.hasPlayer()) return { valid: false };

		return super.status();
	}

	hasPlayer(): boolean {
		var element: HTMLElement = <HTMLElement>this.document.getElementById("mp3Player").querySelector("div.mp3MasterPlayGroup");
		return element && !element.classList.contains("disabled") && !document.getElementById("noMusicInNowPlaying");
	}
}

class PlayerFactory {
	static build(): IPlayer {
		if (/youtube/i.test(location.host)) {
			console.log("factory building youtube");
			return new YoutubePlayer(window);
		}

		if (/spotify/i.test(location.host)) {
			console.log("factory building spotify");
			return new SpotifyPlayer((<HTMLFrameElement>document.getElementById("app-player")).contentWindow);
		}

		if (/amazon/i.test(location.host)) {
			console.log("factory building amazon");
			return new AmazonPlayer(window);
        }

        if (/xbox/i.test(location.host)) {
            console.log("factory building xbox");
            return new XboxPlayer(window);
        }

		console.log("factory building none");
		return new BasePlayer(window);
	}
}

function init() {
	console.log(document.readyState);
	var player: IPlayer = null;

	chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {

		var action = request.action,
			data = request.data;

		if (player === null) {
			player = PlayerFactory.build();
		} else {
			player.update();
		}

		if (action == "get-status") {
			console.log(action, player.status());
			sendResponse(player.status());
		} else if (action == "trigger" && data.length) {
			console.log(action, data);
			player.trigger(data);
		} else {
			console.error("bad request", action, data);
		}

	});
};

init();