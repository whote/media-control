function getPlayerTabs() {
	var totalPlayers = 0,
		amazonInjected = false,
		spotifyInjected = false,
		youtubeInjected = false,
		xboxInjected = false;

	chrome.tabs.query({url: "*://play.spotify.com/*"}, function(tabs) {
		totalPlayers += tabs.length;
		spotifyInjected = true;

		if (youtubeInjected && amazonInjected && xboxInjected) {
			updateBrowserAction(totalPlayers);
		}
	});
	chrome.tabs.query({url: "*://*.youtube.com/*"}, function(tabs) {
		totalPlayers += tabs.length;
		youtubeInjected = true;

		if (spotifyInjected && amazonInjected && xboxInjected) {
			updateBrowserAction(totalPlayers);
		}
	});
	chrome.tabs.query({url: "*://*.amazon.com/*/cloudplayer/*"}, function(tabs) {
		totalPlayers += tabs.length;
		amazonInjected = true;

		if (spotifyInjected && youtubeInjected && xboxInjected) {
			updateBrowserAction(totalPlayers);
		}
	});
	chrome.tabs.query({url: "*://music.xbox.com/*"}, function(tabs) {
		totalPlayers += tabs.length;
		xboxInjected = true;

		if (spotifyInjected && youtubeInjected && amazonInjected) {
			updateBrowserAction(totalPlayers);
		}
	});
};

function updateBrowserAction(totalPlayers) {
	if (totalPlayers === 0) {
		chrome.browserAction.setBadgeText({text: ""});
		//chrome.browserAction.disable();
	} else if (totalPlayers > 0) {
		chrome.browserAction.setBadgeBackgroundColor({color: "#000"});
		chrome.browserAction.setBadgeText({text: totalPlayers.toString()});
		//chrome.browserAction.enable();
	}
};

chrome.runtime.onInstalled.addListener(getPlayerTabs);
chrome.runtime.onStartup.addListener(getPlayerTabs);
chrome.tabs.onCreated.addListener(getPlayerTabs);
chrome.tabs.onRemoved.addListener(getPlayerTabs);
chrome.tabs.onUpdated.addListener(getPlayerTabs);