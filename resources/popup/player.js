// master listener
function buttonListener(event) {
	console.log(event);
	var target = event.target,
		player = event.target,
		tabId = 0;

	while (!/^button$/i.test(target.tagName)) {
		target = target.parentElement;
	}

	while (!player.classList.contains("mc-player")) {
		player = player.parentElement;
	}

	tabId = parseInt(player.dataset.tabId);

	switch (target.dataset.action) {
		case "goto":
			chrome.tabs.update(tabId, {active: true});
			break;

		default:
			chrome.tabs.sendMessage(tabId, {action: "trigger", data: target.dataset.action});
			player.classList.add("loading");
	}
};

document.addEventListener("click", buttonListener, false);