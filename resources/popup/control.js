var TIMEOUT_MS = 1500,
	validTabs = [], 
	spotifyInjected = false, 
	youtubeInjected = false, 
	amazonInjected = false,
	xboxInjected = false;

// http://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery
function  getHashFromString(s) {
	return s.split("").reduce(function(a, b) {
		a = ((a << 5) - a) + b.charCodeAt(0);
		return a & a;
	}, 0);
};

function onDocumentLoad() {
	chrome.tabs.query({url: "*://play.spotify.com/*"}, function(tabs) {
		validTabs = validTabs.concat(tabs);
		spotifyInjected = true;

		if (youtubeInjected && amazonInjected && xboxInjected) {
			createPlayers();
		}
	});
	chrome.tabs.query({url: "*://*.youtube.com/*"}, function(tabs) {
		validTabs = validTabs.concat(tabs);
		youtubeInjected = true;

		if (spotifyInjected && amazonInjected && xboxInjected) {
			createPlayers();
		}
	});
	chrome.tabs.query({url: "*://*.amazon.com/*/cloudplayer/*"}, function(tabs) {
		validTabs = validTabs.concat(tabs);
		amazonInjected = true;

		if (spotifyInjected && youtubeInjected && xboxInjected) {
			createPlayers();
		}
	});
	chrome.tabs.query({url: "*://music.xbox.com/*"}, function(tabs) {
		validTabs = validTabs.concat(tabs);
		xboxInjected = true;

		if (spotifyInjected && youtubeInjected && amazonInjected) {
			createPlayers();
		}
	});
};

function startTimerForPlayer(player) {
	if (player.dataset.timerId) {
		clearTimeout(player.dataset.timerId);
	}

	player.dataset.timerId = setTimeout(function() {
		fillPlayerDetails(player);
	}, TIMEOUT_MS);
};

function createPlayers() {
	var link = document.querySelector("link[rel=import]"),
		playerTemplate = link.import.querySelector("#t-player");

	if (validTabs.length === 0) {
		document.body.classList.add("players-unavailable");
	} else {
		document.body.classList.remove("players-unavailable");
	}

	validTabs.forEach(function(tab, index, array) {
		var clone, player, brand;

		if (/youtube/i.test(tab.url)) {
			brand = "youtube";
		} else if (/spotify/i.test(tab.url)) {
			brand = "spotify";
		} else if (/amazon/i.test(tab.url)) {
			brand = "amazon";
		} else if (/music.xbox.com/i.test(tab.url)) {
			brand = "xbox";
		} else {
			console.error("unknown tab picked up");
			brand = "unknown";
		}

		clone = document.importNode(playerTemplate.content, true);
		brandifyTemplate(clone, brand);

		// attach to DOM
		document.getElementById("mc-players").appendChild(clone);

		player = document.querySelector("#mc-players > :last-child");
		player.id = "mc-player-" + tab.id;
		player.dataset.tabId = tab.id;
		player.dataset.tabIsActive = tab.active;
		player.querySelector("span.url").textContent = tab.url;

		fillPlayerDetails(player); // get details here
	});
};

function brandifyTemplate(cloneNode, brand) {
	var icon = "fa-external-link"; // no amazon or xbox icons
	cloneNode.querySelector("div.add-brand-class").classList.add(brand);

	switch (brand) {
		case "spotify":
			icon = "fa-spotify";
			break;
		case "youtube":
			icon = "fa-youtube-square";
			break;
	}

	cloneNode.querySelector("i.add-brand-icon-class").classList.add(icon);
}

function fillPlayerDetails(player) {
	var marquee, isNewTrack = false;
	chrome.tabs.sendMessage(parseInt(player.dataset.tabId), {action: "get-status"}, function(response) {
		player.classList.remove("loading");
		console.log("response", response);
		if (response && response.valid) {
			player.classList.remove("invalid");

			// track changes
			lastHash = player.dataset.trackHash || "";
			player.dataset.trackHash = getHashFromString(response.data.title);
			isNewTrack = lastHash !== player.dataset.trackHash;
			
			// set player data
			player.querySelector(".currently-playing-thumbnail").style.backgroundImage = "url(" + response.data.imageUrl + ")";
			player.querySelector(".currently-playing.title").title = response.data.title;
			player.querySelector(".currently-playing.title").textContent = response.data.title;
			player.querySelector(".currently-playing.artist").title = response.data.artist;
			player.querySelector(".currently-playing.artist").textContent = response.data.artist;
			// status switches
			if (response.status.paused) {
				player.querySelector(".action-play").style.display = null;
				player.querySelector(".action-pause").style.display = "none";
			} else {
				player.querySelector(".action-play").style.display = "none";
				player.querySelector(".action-pause").style.display = null;
			}

			if (player.dataset.tabIsActive === "true") {
				player.querySelector(".action-goto").style.display = "none";
			} else {
				player.querySelector(".action-goto").style.display = null;
			}

			if (response.status.mute === true) {
				player.querySelector(".action-mute").style.display = "none";
				player.querySelector(".action-unmute").style.display = null;
			} else if (response.status.mute === false) {
				player.querySelector(".action-mute").style.display = null;
				player.querySelector(".action-unmute").style.display = "none";
			} else {
				player.querySelector(".action-mute").style.display = "none";
				player.querySelector(".action-unmute").style.display = "none";
			}

			if (response.status.shuffle === true) {
				player.querySelector(".action-shuffle").classList.add("enabled");
				player.querySelector(".action-shuffle").style.display = null;
			} else if (response.status.shuffle === false) {
				player.querySelector(".action-shuffle").classList.remove("enabled");
				player.querySelector(".action-shuffle").style.display = null;
			} else {
				player.querySelector(".action-shuffle").style.display = "none";
			}

			if (response.status.repeat === true) {
				player.querySelector(".action-repeat").classList.add("enabled");
				player.querySelector(".action-repeat").style.display = null;
			} else if (response.status.repeat === false) {
				player.querySelector(".action-repeat").classList.remove("enabled");
				player.querySelector(".action-repeat").style.display = null;
			} else {
				player.querySelector(".action-repeat").style.display = "none";
			}


			if (response.status.next === true) {
				player.querySelector(".action-next").style.display = null;
			} else {
				player.querySelector(".action-next").style.display = "none";
			}

			if (response.status.previous === true) {
				player.querySelector(".action-previous").style.display = null;
			} else {
				player.querySelector(".action-previous").style.display = "none";
			}

			if (response.status.autoplay === true) {
				player.querySelector(".action-autoplay").classList.add("enabled");
				player.querySelector(".action-autoplay").style.display = null;
			} else if (response.status.autoplay === false) {
				player.querySelector(".action-autoplay").classList.remove("enabled");
				player.querySelector(".action-autoplay").style.display = null;
			} else {
				player.querySelector(".action-autoplay").style.display = "none";
			}

			// Set up marquee
			marquee = player.querySelector(".marquee > .marquee-inner");
			if (isNewTrack) marquee.classList.remove("marquee-enabled"); // reset padding for new measurement

			if (marquee.offsetWidth - marquee.parentNode.offsetWidth > 0) {
				marquee.classList.add("marquee-enabled");
			} else {
				marquee.classList.remove("marquee-enabled");
			}
		} else {
			player.classList.add("invalid");
			// ensure goto tab button visible
			player.querySelector(".action-goto").style.display = null;
		}

		startTimerForPlayer(player);
	});
};

document.body.onload = onDocumentLoad;