var spotify = (function() {
	var initialized = false, clickEvent, document, controls = {}, data = {};

	function init(frameWindow) {
		if (!initialized) {
			initialized = true;

			clickEvent = new MouseEvent("click", {view: frameWindow, bubbles: true, cancelable: true});
			document = frameWindow.document;

			controls["play-pause"] = document.getElementById("play-pause");
			controls.previous = document.getElementById("previous");
			controls.next = document.getElementById("next");
			controls.shuffle = document.getElementById("shuffle"); // on = hasClass("active")
			controls.repeat = document.getElementById("repeat"); // on = hasClass("active")
		}

		if (initialized) updateData();
	};

	function updateData() {
		var image = document.getElementById("cover-art").querySelector("div.sp-image-img");
		data.title = document.getElementById("track-name").textContent.trim();
		data.artist = document.getElementById("track-artist").textContent.trim();
		data.imageUrl = image ? image.style.backgroundImage.replace("url(", "").replace(")", "") : "";
	};

	function trigger(action) {
		var controlElement = controls[action];
		if (controlElement) {
			controlElement.dispatchEvent(clickEvent);
		} else {
			console.error("Action not implemented for spotify", action);
		}
	};

	function getStatus() {
		if (!isValid()) return {valid: false};

		return {
			valid: !controls["play-pause"].classList.contains("disabled"),
			data: data,
			status: {
				shuffle: controls.shuffle.classList.contains("active"),
				repeat: controls.repeat.classList.contains("active"),
				paused: !controls["play-pause"].classList.contains("playing")
			}
		};
	};

	function isValid() {
		return initialized && controls["play-pause"];
	};

	return {
		data: data,
		init: init,
		trigger: trigger,
		status: getStatus
	};
})(),

youtube = (function() {
	var initialized = false, clickEvent, document, controls = {}, data = {};

	function init(frameWindow) {
		if (!initialized) {
			initialized = true;

			clickEvent = new MouseEvent("click", {view: frameWindow, bubbles: true, cancelable: true});
			document = frameWindow.document;

			// Only works with HTML5 player
			controls["play-pause"] = document.querySelector("div.ytp-button-play") || document.querySelector("div.ytp-button-pause");
			controls.previous = document.querySelector("a.prev-playlist-list-item");
			controls.next = document.querySelector("a.next-playlist-list-item");
			controls.shuffle = document.querySelector("button.shuffle-playlist");
			controls.autoplay = document.querySelector("button.toggle-autoplay");
			controls.mute = document.querySelector("div.ytp-button-volume");
		}

		if (initialized) updateData();
	};

	function updateData() {
		var hasPlayer = hasHTML5Player();
		data.title = hasPlayer ? document.getElementById("eow-title").textContent.trim() : "";
		data.artist = hasPlayer ? document.querySelector("a.yt-user-name").textContent.trim() : "";
		data.imageUrl = hasPlayer ? document.querySelector("meta[property='og:image']").content : "";
	};

	function trigger(action) {
		var controlElement = controls[action];
		if (controlElement) {
			controlElement.dispatchEvent(clickEvent);
		} else {
			console.error("Action not implemented for youtube", action);
		}
	};

	function getStatus() {
		if (!isValid()) return {valid: false};

		return {
			valid: hasHTML5Player(),
			data: data,
			status: {
				autoplay: controls.autoplay ? controls.autoplay.classList.contains("yt-uix-button-toggled") : false,
				paused: controls["play-pause"] ? controls["play-pause"].classList.contains("ytp-button-play") : false,
				muted: controls.mute ? controls.mute.dataset.value === "off" : false
			},
			isPlaylist: !!controls.autoplay,
		};
	};

	function hasHTML5Player() {
		var player = document.getElementById("movie_player"),
			container = document.getElementById("player"),
			hasPlayer = player && container ? player.classList.contains("html5-video-player") && !container.classList.contains("off-screen") : false;
		return hasPlayer;
	}

	function isValid() {
		return initialized && controls["play-pause"];
	};

	return {
		data: data,
		init: init,
		trigger: trigger,
		status: getStatus
	};
})();

console.log("mc-alive");

function init() {
	if (/youtube/i.test(location.host)) {
		youtube.init(window);
		console.log("youtube");
	}
	if (/spotify/i.test(location.host)) {
		spotify.init(document.getElementById("app-player").contentWindow);
		console.log("spotify");
	}
};

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	console.log(request, sender);
	init();

	var player = youtube.status().valid ? youtube : spotify,
		action = request.action,
		data = request.data;

	if (!action) {
		console.error("bad request");
	}

	if (action === "get-status") {
		sendResponse(player.status());
	} else if (action === "trigger" && data.length) {
		player.trigger(data);
	}
});