interface DocumentEvent {
	createEvent(eventInterface: "MouseEvent"): MouseEvent;
}

interface MouseEvent extends UIEvent {
	initMouseEvent(typeArg: string, bubblesArg: boolean, cancelableArg: boolean, view: Window): void;
}